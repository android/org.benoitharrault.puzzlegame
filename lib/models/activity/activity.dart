import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puzzlegame/config/application_config.dart';
import 'package:puzzlegame/data/fetch_data_helper.dart';
import 'package:puzzlegame/models/activity/moving_tile.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,
    this.shufflingInProgress = false,

    // Base data
    required this.image,
    this.tiles = const [],

    // Game data
    this.movesCount = 0,
    this.displayTip = false,
    this.tileSize = 0.0,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;
  bool shufflingInProgress;

  // Base data
  String image;
  List<MovingTile> tiles;

  // Game data
  int movesCount;
  bool displayTip;
  double tileSize;

  factory Activity.createEmpty() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      image: '',
      tiles: [],
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    final String image = FetchDataHelper().getRandomItem();

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // Base data
      image: image,
    );
  }

  bool get canBeResumed => isStarted && !isFinished;

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('    shufflingInProgress: $shufflingInProgress');
    printlog('  Base data');
    printlog('    image: $image');
    printlog('  Game data');
    printlog('    movesCount: $movesCount');
    printlog('    displayTip: $displayTip');
    printlog('    tileSize: $tileSize');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      'shufflingInProgress': shufflingInProgress,
      // Base data
      'image': image,
      'tiles': tiles,
      // Game data
      'movesCount': movesCount,
      'displayTip': displayTip,
      'tileSize': tileSize,
    };
  }
}
