import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puzzlegame/cubit/activity/activity_cubit.dart';
import 'package:puzzlegame/models/activity/activity.dart';

class GameTopWidget extends StatelessWidget {
  const GameTopWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return currentActivity.shufflingInProgress
            ? const SizedBox.shrink()
            : Text(
                '${currentActivity.movesCount}',
                style: const TextStyle(
                  fontSize: 45,
                  fontWeight: FontWeight.w600,
                ),
              );
      },
    );
  }
}
