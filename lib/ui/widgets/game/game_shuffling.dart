import 'package:flutter/material.dart';

class GameShufflingWidget extends StatelessWidget {
  const GameShufflingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          '⏳',
          style: TextStyle(
            fontSize: 60,
            fontWeight: FontWeight.w600,
            color: Colors.black,
          ),
        ),
        SizedBox(height: 20),
        CircularProgressIndicator(),
      ],
    );
  }
}
