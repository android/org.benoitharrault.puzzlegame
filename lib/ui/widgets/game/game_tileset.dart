import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puzzlegame/config/application_config.dart';

import 'package:puzzlegame/cubit/activity/activity_cubit.dart';
import 'package:puzzlegame/models/activity/activity.dart';
import 'package:puzzlegame/models/activity/moving_tile.dart';
import 'package:puzzlegame/ui/widgets/game/game_tile.dart';

class GameTilesetwidget extends StatelessWidget {
  const GameTilesetwidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final Color borderColor = currentActivity.isFinished ? Colors.green : Colors.orange;
        final int tilesCount = currentActivity.activitySettings
            .getAsInt(ApplicationConfig.parameterCodeTilesetSize);

        final List<MovingTile> tiles = currentActivity.tiles;
        int tileIndex = 0;

        return Container(
          margin: const EdgeInsets.all(8),
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
            color: borderColor,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(
              color: borderColor,
              width: 8,
            ),
          ),
          child: Table(
            defaultColumnWidth: const IntrinsicColumnWidth(),
            border: TableBorder.all(
              color: Colors.black,
              style: BorderStyle.solid,
              width: 2,
            ),
            children: [
              for (int row = 0; row < tilesCount; row++)
                TableRow(
                  children: [
                    for (int col = 0; col < tilesCount; col++)
                      Column(
                        children: [
                          GameTileWidget(
                            tile: tiles[tileIndex++],
                          )
                        ],
                      ),
                  ],
                ),
            ],
          ),
        );
      },
    );
  }
}
