import 'package:flutter/material.dart';

class Tile {
  final Image image;
  double size;
  final int originalCol;
  final int originalRow;

  Tile({
    required this.image,
    required this.size,
    required this.originalCol,
    required this.originalRow,
  });

  @override
  String toString() {
    return '$Tile(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'size': size,
      'originalCol': originalCol,
      'originalRow': originalRow,
    };
  }
}
