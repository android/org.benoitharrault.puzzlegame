import 'dart:async';
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:image/image.dart' as imglib;

import 'package:puzzlegame/config/application_config.dart';
import 'package:puzzlegame/models/activity/activity.dart';
import 'package:puzzlegame/models/activity/moving_tile.dart';

part 'activity_state.dart';

class ActivityCubit extends HydratedCubit<ActivityState> {
  ActivityCubit()
      : super(ActivityState(
          currentActivity: Activity.createEmpty(),
        ));

  void updateState(Activity activity) {
    emit(ActivityState(
      currentActivity: activity,
    ));
  }

  void refresh() {
    final Activity activity = Activity(
      // Settings
      activitySettings: state.currentActivity.activitySettings,
      // State
      isRunning: state.currentActivity.isRunning,
      isStarted: state.currentActivity.isStarted,
      isFinished: state.currentActivity.isFinished,
      animationInProgress: state.currentActivity.animationInProgress,
      shufflingInProgress: state.currentActivity.shufflingInProgress,
      // Base data
      image: state.currentActivity.image,
      tiles: state.currentActivity.tiles,
      // Game data
      movesCount: state.currentActivity.movesCount,
      displayTip: state.currentActivity.displayTip,
      tileSize: state.currentActivity.tileSize,
    );
    // game.dump();

    updateState(activity);
  }

  void startNewActivity(BuildContext context) {
    final ActivitySettingsCubit activitySettingsCubit =
        BlocProvider.of<ActivitySettingsCubit>(context);

    final Activity newActivity = Activity.createNew(
      // Settings
      activitySettings: activitySettingsCubit.state.settings,
    );

    newActivity.dump();

    updateState(newActivity);
    refresh();

    state.currentActivity.isRunning = true;
    state.currentActivity.shufflingInProgress = true;
    refresh();

    Timer(const Duration(seconds: 1), () {
      splitImageInTiles();
    });
  }

  bool canBeResumed() {
    return state.currentActivity.canBeResumed;
  }

  void quitActivity() {
    state.currentActivity.isRunning = false;
    refresh();
  }

  void resumeSavedActivity() {
    state.currentActivity.isRunning = true;
    refresh();
  }

  void deleteSavedActivity() {
    state.currentActivity.isRunning = false;
    state.currentActivity.isFinished = true;
    refresh();
  }

  void updateTileSize(double tileSize) {
    if (tileSize != state.currentActivity.tileSize) {
      state.currentActivity.tileSize = tileSize;
      for (var i = 0; i < state.currentActivity.tiles.length; i++) {
        state.currentActivity.tiles[i].size = tileSize;
      }
      refresh();
    }
  }

  void toggleDisplayTipImage() {
    state.currentActivity.displayTip = !state.currentActivity.displayTip;
    refresh();
  }

  void incrementMovesCount() {
    state.currentActivity.movesCount++;
    refresh();
  }

  bool checkTilesetIsCleared() {
    for (MovingTile tile in state.currentActivity.tiles) {
      if (!tile.isCorrect()) {
        return false;
      }
    }
    return true;
  }

  void swapTiles(List<int> tile1, List<int> tile2) {
    state.currentActivity.isStarted = true;

    final int indexTile1 = state.currentActivity.tiles.indexWhere(
        (tile) => ((tile.currentCol == tile1[0]) && (tile.currentRow == tile1[1])));
    final int indexTile2 = state.currentActivity.tiles.indexWhere(
        (tile) => ((tile.currentCol == tile2[0]) && (tile.currentRow == tile2[1])));

    final MovingTile swap = state.currentActivity.tiles[indexTile1];
    state.currentActivity.tiles[indexTile1] = state.currentActivity.tiles[indexTile2];
    state.currentActivity.tiles[indexTile2] = swap;

    final int swapCol = state.currentActivity.tiles[indexTile1].currentCol;
    state.currentActivity.tiles[indexTile1].currentCol =
        state.currentActivity.tiles[indexTile2].currentCol;
    state.currentActivity.tiles[indexTile2].currentCol = swapCol;

    final int swapRow = state.currentActivity.tiles[indexTile1].currentRow;
    state.currentActivity.tiles[indexTile1].currentRow =
        state.currentActivity.tiles[indexTile2].currentRow;
    state.currentActivity.tiles[indexTile2].currentRow = swapRow;

    incrementMovesCount();
    if (checkTilesetIsCleared()) {
      state.currentActivity.isFinished = true;
    }

    refresh();
  }

  Future<void> splitImageInTiles() async {
    final String imageAsset = 'assets/images/${state.currentActivity.image}.png';
    final Uint8List imageData = (await rootBundle.load(imageAsset)).buffer.asUint8List();

    final int tilesCount = state.currentActivity.activitySettings
        .getAsInt(ApplicationConfig.parameterCodeTilesetSize);

    final imglib.Image image = imglib.decodeImage(imageData) ??
        imglib.Image.fromBytes(
          height: 1,
          width: 1,
          bytes: Uint8List.fromList([]).buffer,
        );

    int x = 0, y = 0;
    final int width = (image.width / tilesCount).round();
    final int height = (image.height / tilesCount).round();

    final List<MovingTile> tiles = [];
    for (int i = 0; i < tilesCount; i++) {
      for (int j = 0; j < tilesCount; j++) {
        final Uint8List tileData = Uint8List.fromList(imglib.encodeJpg(imglib.copyCrop(
          image,
          x: x,
          y: y,
          width: width,
          height: height,
        )));

        tiles.add(MovingTile(
          currentCol: j,
          currentRow: i,
          image: Image.memory(tileData),
          size: state.currentActivity.tileSize,
          originalCol: j,
          originalRow: i,
        ));

        x += width;
      }
      x = 0;
      y += height;
    }

    state.currentActivity.tiles = tiles;
    shuffleTiles();

    state.currentActivity.shufflingInProgress = false;
    refresh();
  }

  void shuffleTiles() {
    final Random random = Random();

    final List<MovingTile> tiles = state.currentActivity.tiles;
    final int tilesCount = tiles.length;

    for (int i = 0; i < (10 * tilesCount); i++) {
      final int indexTile1 = random.nextInt(tilesCount);
      final int indexTile2 = random.nextInt(tilesCount);

      final MovingTile swap = tiles[indexTile1];
      tiles[indexTile1] = tiles[indexTile2];
      tiles[indexTile2] = swap;

      final int swapCol = tiles[indexTile1].currentCol;
      tiles[indexTile1].currentCol = tiles[indexTile2].currentCol;
      tiles[indexTile2].currentCol = swapCol;

      final int swapRow = tiles[indexTile1].currentRow;
      tiles[indexTile1].currentRow = tiles[indexTile2].currentRow;
      tiles[indexTile2].currentRow = swapRow;
    }

    state.currentActivity.tiles = tiles;
  }

  @override
  ActivityState? fromJson(Map<String, dynamic> json) {
    final Activity currentActivity = json['currentActivity'] as Activity;

    return ActivityState(
      currentActivity: currentActivity,
    );
  }

  @override
  Map<String, dynamic>? toJson(ActivityState state) {
    return <String, dynamic>{
      'currentActivity': state.currentActivity.toJson(),
    };
  }
}
