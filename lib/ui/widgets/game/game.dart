import 'package:flutter/material.dart';

import 'package:puzzlegame/ui/game/game_top.dart';
import 'package:puzzlegame/ui/widgets/game/game_tileset.dart';

class GameWidget extends StatelessWidget {
  const GameWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SizedBox(height: 8),
        const GameTopWidget(),
        const SizedBox(height: 2),
        Expanded(
          child: Container(
            margin: const EdgeInsets.all(4),
            padding: const EdgeInsets.all(4),
            child: const Column(
              children: [
                GameTilesetwidget(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
