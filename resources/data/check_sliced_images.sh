#!/usr/bin/env bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
BASE_DIR="$(dirname "${CURRENT_DIR}")"

ASSETS_BASE_FOLDER="${BASE_DIR}/assets"
ASSETS_FILE="${ASSETS_BASE_FOLDER}/files/images.json"
IMAGES_ASSETS_FOLDER="${ASSETS_BASE_FOLDER}/images"

if [[ ! -f "${ASSETS_FILE}" ]]; then
  echo "Missing json assets file ${ASSETS_FILE}"
  echo "Abort."
  exit 1
fi

# Encode images list to base64 to inject into javascript code
IMAGES_LIST_ENCODED="$(cat "${ASSETS_FILE}" | base64 -w0)"

HTML_OUTPUT_FiLE="${CURRENT_DIR}/index.html"

HTML_CONTENT="
<html>
  <head>
    <title></title>
    <style>
      #images-board {
        width: 100%;
        border-collapse: collapse;
      }
      #images-board td {
        padding: 5px;
      }
      #images-board span {
        font-family: monospace;
        white-space: pre;
      }
      #images-board img {
        width: 100%;
        border: solid 2px #222;
      }
      #images-board div {
        position: relative;
      }
      #images-board div svg {
        position: absolute;
        top: 3px;
        left: 3px;
      }
    </style>
  </head>
  <body>
    <table id=\"images-board\"></table>
    <script>
      const images_json_base64 = '${IMAGES_LIST_ENCODED}';
      const images_json = atob(images_json_base64);
      const images = JSON.parse(images_json);

      const lineStyle = 'stroke=\"orange\" stroke-width=\"2\"';
      const size = 300;

      function lines(count) {
        svg = '';
        for (var i = 0; i <= count; i++) {
          z = (size / count) * i;
          svg += '<line x1=\"0\" x2=\"' + size + '\" y1=\"' + z + '\" y2=\"' + z + '\" ' + lineStyle + '/>';
          svg += '<line x1=\"' + z + '\" x2=\"' + z + '\" y1=\"0\" y2=\"' + size + '\" ' + lineStyle + '/>';
        }
        return svg;
      }

      const mask_3 = '<svg viewBox=\"0 0 ' + size + ' ' + size + '\">' + lines(3) + '</svg>';
      const mask_4 = '<svg viewBox=\"0 0 ' + size + ' ' + size + '\">' + lines(4) + '</svg>';
      const mask_5 = '<svg viewBox=\"0 0 ' + size + ' ' + size + '\">' + lines(5) + '</svg>';

      var table = document.getElementById('images-board');
      images.images.forEach(function (image) {
        if (image !== '') {
          imageUrl = '../assets/images/' + image + '.png';

          var cell1 = document.createElement('td');
          var cell2 = document.createElement('td');
          var cell3 = document.createElement('td');
          var cell4 = document.createElement('td');

          cell1.innerHTML = '<span>' + image + '</span>';
          cell2.innerHTML = '<div><img src=\"' + imageUrl + '\">' + mask_3 + '</div>';
          cell3.innerHTML = '<div><img src=\"' + imageUrl + '\">' + mask_4 + '</div>';
          cell4.innerHTML = '<div><img src=\"' + imageUrl + '\">' + mask_5 + '</div>';

          var line = document.createElement('tr');
          line.appendChild(cell1);
          line.appendChild(cell2);
          line.appendChild(cell3);
          line.appendChild(cell4);

          table.appendChild(line);
        }
      });
    </script>
  </body>
</html>
"

echo "${HTML_CONTENT}" >"${HTML_OUTPUT_FiLE}"

echo "done."
