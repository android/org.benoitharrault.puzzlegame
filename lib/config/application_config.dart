import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puzzlegame/cubit/activity/activity_cubit.dart';

import 'package:puzzlegame/ui/pages/game.dart';

import 'package:puzzlegame/ui/parameters/parameter_painter_tileset_size.dart';

class ApplicationConfig {
  // activity parameter: tileset size
  static const String parameterCodeTilesetSize = 'activity.tilesetSize';
  static const String tilesetSizeValueSmall = '3x3';
  static const String tilesetSizeValueMedium = '4x4';
  static const String tilesetSizeValueLarge = '5x5';

  // activity parameter: image picking type
  static const String parameterCodeImageName = 'activity.imageName';
  static const String imageNameRandomlyPicked = 'random';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Puzzle',
    activitySettings: [
      // tileset size
      ApplicationSettingsParameter(
        code: parameterCodeTilesetSize,
        values: [
          ApplicationSettingsParameterItemValue(
            value: tilesetSizeValueSmall,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: tilesetSizeValueMedium,
            isDefault: true,
            color: Colors.orange,
          ),
          ApplicationSettingsParameterItemValue(
            value: tilesetSizeValueLarge,
            color: Colors.red,
          ),
        ],
        customPainter: (context, value) => ParameterPainterTilesetSize(
          context: context,
          value: value,
        ),
        intValueGetter: (String value) => int.parse(value.split('x')[0]),
      ),

      // image name
      ApplicationSettingsParameter(
        code: parameterCodeImageName,
        values: [
          ApplicationSettingsParameterItemValue(
            value: imageNameRandomlyPicked,
            isDefault: true,
          ),
        ],
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );
}
