import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puzzlegame/cubit/activity/activity_cubit.dart';
import 'package:puzzlegame/models/activity/activity.dart';

class GameTipWidget extends StatelessWidget {
  const GameTipWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final String asset = 'assets/images/${currentActivity.image}.png';
        precacheImage(AssetImage(asset), context);

        return TextButton(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(4),
              border: Border.all(
                color: Colors.blue,
                width: 4,
              ),
            ),
            child: Image(
              height: currentActivity.tileSize,
              image: AssetImage(
                currentActivity.displayTip ? asset : 'assets/ui/tip_hidden.png',
              ),
              fit: BoxFit.fill,
            ),
          ),
          onPressed: () {
            BlocProvider.of<ActivityCubit>(context).toggleDisplayTipImage();
          },
        );
      },
    );
  }
}
