import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puzzlegame/cubit/activity/activity_cubit.dart';
import 'package:puzzlegame/models/activity/activity.dart';
import 'package:puzzlegame/ui/widgets/game/game_tip.dart';

class GameBottomWidget extends StatelessWidget {
  const GameBottomWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return (!currentActivity.isFinished && !currentActivity.shufflingInProgress)
            ? const GameTipWidget()
            : const SizedBox.shrink();
      },
    );
  }
}
