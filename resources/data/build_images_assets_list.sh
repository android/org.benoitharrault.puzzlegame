#!/usr/bin/env bash

command -v jq >/dev/null 2>&1 || {
  echo >&2 "I require jq (json parser) but it's not installed. Aborting."
  exit 1
}

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
BASE_DIR="$(dirname "$(dirname "${CURRENT_DIR}")")"

ASSETS_BASE_FOLDER="${BASE_DIR}/assets"
IMAGES_ASSETS_FOLDER="${ASSETS_BASE_FOLDER}/images"

IMAGES_CACHE_FOLDER="${CURRENT_DIR}/cache"
IMAGES_OPTIMIZED_FOLDER="${IMAGES_CACHE_FOLDER}/02_optimized_images"

echo "Cleaning empty/temp files..."
find "${IMAGES_OPTIMIZED_FOLDER}" -type f -name "*.png" -empty -exec rm {} \;
find "${IMAGES_OPTIMIZED_FOLDER}" -type f -name "*.tmp*" -exec rm {} \;

echo "Move new optimized images..."
find "${IMAGES_OPTIMIZED_FOLDER}" -type f -name "*.png" -exec mv -v {} "${IMAGES_ASSETS_FOLDER}" \;

echo "Building assets json file..."

FILES="$(find "${IMAGES_ASSETS_FOLDER}" -type f -name "*.png" | sed "s|^${IMAGES_OPTIMIZED_FOLDER}/||g" | sort)"

echo "Injecting json file in game_data.dart..."

GAME_DATA_DART_FILE="${BASE_DIR}/lib/data/game_data.dart"
echo "class GameData {" >"${GAME_DATA_DART_FILE}"
echo "  static const Map<String, dynamic> data = {" >>"${GAME_DATA_DART_FILE}"
echo "    \"images\": [" >>"${GAME_DATA_DART_FILE}"
while read -r FILE; do
  FILE_CODE="$(basename "${FILE%.*}")"
  if [[ -n "${FILE}" ]]; then
    echo "- ${FILE_CODE}"
    echo "      \"${FILE_CODE}\"," >>"${GAME_DATA_DART_FILE}"
  fi
done < <(echo "${FILES}")
echo "    ]" >>"${GAME_DATA_DART_FILE}"
echo "  };" >>"${GAME_DATA_DART_FILE}"

echo "}" >>"${GAME_DATA_DART_FILE}"

echo "Formatting dart source code file..."
dart format "${GAME_DATA_DART_FILE}"

echo "done."
