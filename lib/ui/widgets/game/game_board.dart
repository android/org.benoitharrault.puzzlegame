import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:puzzlegame/config/application_config.dart';

import 'package:puzzlegame/cubit/activity/activity_cubit.dart';
import 'package:puzzlegame/models/activity/activity.dart';
import 'package:puzzlegame/ui/widgets/game/game_shuffling.dart';
import 'package:puzzlegame/ui/widgets/game/game_tileset.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocBuilder<ActivityCubit, ActivityState>(
        builder: (BuildContext context, ActivityState activityState) {
          final Activity currentActivity = activityState.currentActivity;

          BlocProvider.of<ActivityCubit>(context).updateTileSize(
              (MediaQuery.of(context).size.width - 60) /
                  currentActivity.activitySettings
                      .getAsInt(ApplicationConfig.parameterCodeTilesetSize));

          if (currentActivity.shufflingInProgress) {
            return const GameShufflingWidget();
          } else {
            return const GameTilesetwidget();
          }
        },
      ),
    );
  }
}
