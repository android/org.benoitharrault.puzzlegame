import 'package:puzzlegame/models/activity/tile.dart';

class MovingTile extends Tile {
  int currentCol;
  int currentRow;

  MovingTile({
    required super.image,
    required super.size,
    required super.originalCol,
    required super.originalRow,
    required this.currentCol,
    required this.currentRow,
  });

  bool isCorrect() {
    return ((currentRow == originalRow) && (currentCol == originalCol));
  }

  @override
  String toString() {
    return '$Tile(${toJson()})';
  }

  @override
  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'size': size,
      'originalCol': originalCol,
      'originalRow': originalRow,
      'currentRow': currentRow,
      'currentCol': currentCol,
    };
  }
}
