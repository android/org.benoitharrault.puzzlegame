import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puzzlegame/cubit/activity/activity_cubit.dart';
import 'package:puzzlegame/models/activity/moving_tile.dart';

class GameTileWidget extends StatelessWidget {
  const GameTileWidget({super.key, required this.tile});

  final MovingTile tile;

  @override
  Widget build(BuildContext context) {
    return DragTarget<List<int>>(
      builder: (
        BuildContext context,
        List<dynamic> accepted,
        List<dynamic> rejected,
      ) {
        return Container(
          height: tile.size,
          width: tile.size,
          color: Colors.cyan,
          child: Draggable<List<int>>(
            data: [
              tile.currentCol,
              tile.currentRow,
            ],

            // Widget when draggable is being dragged
            feedback: staticTile(),

            // Widget to display on original place when being dragged
            childWhenDragging: Container(
              height: tile.size,
              width: tile.size,
              color: Colors.pinkAccent,
            ),

            // Widget when draggable is stationary
            child: staticTile(),
          ),
        );
      },
      onAcceptWithDetails: (DragTargetDetails<List<int>> source) {
        BlocProvider.of<ActivityCubit>(context)
            .swapTiles([tile.currentCol, tile.currentRow], source.data);
      },
    );
  }

  Container staticTile() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.black,
        border: Border.all(
          color: Colors.black,
          width: 1,
        ),
      ),
      child: Image(
        image: tile.image.image,
        width: tile.size,
        height: tile.size,
        fit: BoxFit.fill,
      ),
    );
  }
}
