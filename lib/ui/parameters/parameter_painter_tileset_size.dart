import 'dart:math';

import 'package:flutter/material.dart';

class ParameterPainterTilesetSize extends CustomPainter {
  const ParameterPainterTilesetSize({
    required this.context,
    required this.value,
  });

  final BuildContext context;
  final String value;

  @override
  void paint(Canvas canvas, Size size) {
    // force square
    final double canvasSize = min(size.width, size.height);

    final int gridSize = int.parse(value.split('x')[0]);

    final paint = Paint();
    paint.strokeJoin = StrokeJoin.round;
    paint.strokeWidth = 3;

    // Mini grid
    // TODO: add image
    final borderColor = Colors.grey.shade800;

    final drawSize = canvasSize * 0.7;

    final double cellSize = drawSize / gridSize;
    final double originX = (canvasSize - gridSize * cellSize) / 2;
    final double originY = (canvasSize - gridSize * cellSize) / 2;

    for (int row = 0; row < gridSize; row++) {
      for (int col = 0; col < gridSize; col++) {
        final Offset topLeft = Offset(originX + col * cellSize, originY + row * cellSize);
        final Offset bottomRight = topLeft + Offset(cellSize, cellSize);

        paint.color = Colors.grey.shade200;
        paint.style = PaintingStyle.fill;
        canvas.drawRect(Rect.fromPoints(topLeft, bottomRight), paint);

        paint.color = borderColor;
        paint.style = PaintingStyle.stroke;
        canvas.drawRect(Rect.fromPoints(topLeft, bottomRight), paint);
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
