import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:puzzlegame/data/game_data.dart';

class FetchDataHelper {
  FetchDataHelper();

  final List<String> _images = [];
  List<String> get images => _images;

  void init() {
    try {
      final List<String> rawImages = GameData.data['images'] as List<String>;
      for (var imageCode in rawImages) {
        _images.add(imageCode.toString());
      }
    } catch (e) {
      printlog("$e");
    }
  }

  List<String> getRandomItems(int count) {
    if (_images.isEmpty) {
      init();
    }

    List<String> shuffleableList = [];
    for (var i = 0; i < _images.length; i++) {
      shuffleableList.add(_images[i]);
    }

    shuffleableList.shuffle();

    return shuffleableList.sublist(0, count);
  }

  String getRandomItem() {
    return getRandomItems(1).first;
  }
}
